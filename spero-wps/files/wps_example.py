import sys, subprocess, os, logging
import json, shutil, requests, glob
from datetime import datetime
from lxml import etree as et

def get_logger(file=None):
    logger = logging.getLogger('wps_mockup')
    logger.setLevel(logging.DEBUG)
    if file == None:
        hdl = logging.StreamHandler(sys.stdout)
    else:
        hdl = logging.FileHandler(file)
    hdl.setLevel(logging.DEBUG)
    fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    hdl.setFormatter(fmt)
    logger.addHandler(hdl)
    return logger

import zoo

def wps_example(conf, inputs, outputs):

    pid = conf['lenv']['usid']
    tmp_path = conf['main']['tmpPath']
    tmp_url = conf['main']['tmpUrl']

    logger = get_logger(file=os.path.join(tmp_path, pid + '-custom.log'))
    logger.info("ZOO module imported successfully. Running as a WPS service ...")
    logger.debug("ZOO Context: \n%s" % json.dumps(conf, indent=4))
    logger.debug("ZOO Inputs: \n%s" % json.dumps(inputs, indent=4))
    logger.debug("ZOO Outputs: \n%s" % json.dumps(outputs, indent=4))
    logger.debug('Running Environment: {}'.format(os.environ))

    # create working environment in tmp_path
    try:
        _dwn_dir = os.path.join(tmp_path, "{}-input".format(pid))
        os.mkdir(_dwn_dir)
        _out_dir = os.path.join(tmp_path, "{}-output".format(pid))
        os.mkdir(_out_dir)
        _work_dir = os.path.join(tmp_path, "{}-work".format(pid))
        os.mkdir(_work_dir)
    except Exception as e:
        conf['lenv']['message'] = "{} || {}".format(e.message, e.args)
        return zoo.SERVICE_FAILED
    
    try:
        _bbox = et.fromstring(inputs['area_of_interest']['value'].replace('ows:', ''))
        _lowc = _bbox.xpath('//LowerCorner/text()')[0]
        _minx = _lowc.split(" ")[0]
        _miny = _lowc.split(" ")[1]
        logger.debug('BBox: decoded minx ({}) and miny ({})'.format(_minx, _miny))
        _upperc = _bbox.xpath('//UpperCorner/text()')[0]
        _maxx = _upperc.split(" ")[0]
        _maxy = _upperc.split(" ")[1]
        logger.debug('BBox: decoded maxx ({}) and maxy ({})'.format(_maxx, _maxy))
        _bb = box(float(_minx), float(_miny), float(_maxx), float(_maxy))
        logger.debug('BBox: decoded WKT geometry: {}'.format(_bb.wkt))
    except Exception as e:
        conf['lenv']['message'] = "AOI bounding box couldn't be decoded! ({}:{})".format(e.message, e.args)
        return zoo.SERVICE_FAILED

   try:
        _event_ts = inputs['time_of_interest']['value']
    except:
        logger.error('time_of_interest argument is missing')
        conf['lenv']['message'] = 'time_of_interest argument is missing'
        return zoo.SERVICE_FAILED

        
    outputs['Result']['value'] = '{"aoi": {}, "toi": {}}'.format(_bb.wkt, _event_ts)
    return zoo.SERVICE_SUCCEEDED

