#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

CRUDINI=/usr/bin/crudini
CFG=/etc/zoo-project/main.cfg

_ZOO_SERVER_ADDRESS=${ZOO_SERVER_ADDRESS:-http://localhost/cgi-bin/zoo_loader.cgi}
_ZOO_TMP_URL=${ZOO_TMP_URL:-https://localhost/temp/}
_ZOO_TMP_PATH=${ZOO_TMP_PATH:-"/tmp"}
_ZOO_CORS=${ZOO_CORS:-false}
_ZOO_CORS_ALLOW_ORIGIN=${ZOO_CORS_ALLOW_ORIGIN:-"*"}
_ZOO_CORS_ALLOW_METHODS=${ZOO_CORS_ALLOW_METHODS:-"POST"}
_ZOO_IDENTIFICATION_KEYWORDS=${ZOO_IDENTIFICATION_KEYWORDS:-"Zoo-Project,SPERO,WPS,GIS"}
_ZOO_IDENTIFICATION_TITLE=${ZOO_IDENTIFICATION_TITLE:-"SPERO Customised Zoo Instance"}
_ZOO_IDENTIFICATION_ABSTRACT=${ZOO_IDENTIFICATION_ABSTRACT:-"This is an instance of Zoo-Project, the Open WPS platform. The instance was customised as part of the SPERO project"}
_ZOO_PROVIDER_INDIVIDUAL_NAME=${ZOO_PROVIDER_INDIVIDUAL_NAME:-"Silviu Panica"}
_ZOO_PROVIDER_POSITION_NAME=${ZOO_PROVIDER_POSITION_NAME:-"Developer"}
_ZOO_PROVIDER_NAME=${ZOO_PROVIDER_NAME:-"Terrasigna SRL"}
_ZOO_PROVIDER_ADDRESS_ADMINISTRATIVE_AREA=${ZOO_PROVIDER_ADDRESS_ADMINISTRATIVE_AREA:-"Bucharest"}
_ZOO_PROVIDER_ADDRESS_DELIVERY_POINT=${ZOO_PROVIDER_ADDRESS_DELIVERY_POINT:-"3 Logofat Luca Stroici Street"}
_ZOO_PROVIDER_ADDRESS_COUNTRY=${ZOO_PROVIDER_ADDRESS_COUNTRY:-ro}
_ZOO_PROVIDER_SITE=${ZOO_PROVIDER_SITE:-"www.terrasigna.ro"}
_ZOO_PROVIDER_ADDRESS_CITY=${ZOO_PROVIDER_ADDRESS_CITY:-"Bucharest"}
_ZOO_PROVIDER_ADDRESS_ZIP=${ZOO_PROVIDER_ADDRESS_ZIP:-"020581"}
_ZOO_PROVIDER_EMAIL=${ZOO_PROVIDER_EMAIL:-"silviu@solsys.ro"}

${CRUDINI} --set ${CFG} main serverAddress "${_ZOO_SERVER_ADDRESS}"
${CRUDINI} --set ${CFG} main tmpUrl "${_ZOO_TMP_URL}"
${CRUDINI} --set ${CFG} main tmpPath "${_ZOO_TMP_PATH}"
${CRUDINI} --set ${CFG} main cors "${_ZOO_CORS}"
${CRUDINI} --set ${CFG} main version "2.0.0"

${CRUDINI} --set ${CFG} headers "Access-Control-Allow-Origin" "${_ZOO_CORS_ALLOW_ORIGIN}"
${CRUDINI} --set ${CFG} headers "Access-Control-Allow-Methods" "${_ZOO_CORS_ALLOW_METHODS}"

${CRUDINI} --set ${CFG} identification keywords "${_ZOO_IDENTIFICATION_KEYWORDS}"
${CRUDINI} --set ${CFG} identification title "${_ZOO_IDENTIFICATION_TITLE}"
${CRUDINI} --set ${CFG} identification abstract "${_ZOO_IDENTIFICATION_ABSTRACT}"

${CRUDINI} --set ${CFG} provider individualName "${_ZOO_PROVIDER_INDIVIDUAL_NAME}"
${CRUDINI} --set ${CFG} provider positionName "${_ZOO_PROVIDER_POSITION_NAME}"
${CRUDINI} --set ${CFG} provider providerName "${_ZOO_PROVIDER_NAME}"
${CRUDINI} --set ${CFG} provider addressAdministrativeArea "${_ZOO_PROVIDER_ADDRESS_ADMINISTRATIVE_AREA}"
${CRUDINI} --set ${CFG} provider addressDeliveryPoint "${_ZOO_PROVIDER_ADDRESS_DELIVERY_POINT}"
${CRUDINI} --set ${CFG} provider addressCountry "${_ZOO_PROVIDER_ADDRESS_COUNTRY}"
${CRUDINI} --set ${CFG} provider providerSite "${_ZOO_PROVIDER_SITE}"
${CRUDINI} --set ${CFG} provider providerCity "${_ZOO_PROVIDER_ADDRESS_CITY}"
${CRUDINI} --set ${CFG} provider addressPostalCode "${_ZOO_PROVIDER_ADDRESS_ZIP}"
${CRUDINI} --set ${CFG} provider addressElectronicMailAddress "${_ZOO_PROVIDER_EMAIL}"

cat <<EOF > /etc/apache2/conf-available/zoo-static.conf
Header always set Access-Control-Allow-Origin "*"
Header always set Access-Control-Allow-Methods "POST, GET, OPTIONS"
Alias "/wps-output" "/mnt/wps-output"
<Directory "/mnt/wps-output">
    Options +Indexes
    Order allow,deny
    Allow from all
    Require all granted
</Directory>
EOF

chown www-data:www-data /mnt/wps-output
chown -R www-data:www-data /var/www

/usr/sbin/a2enmod headers >/dev/null
/usr/sbin/a2enconf zoo-static >/dev/null

echo "[Zoo-Docker] Configuration Updated"
echo "[Zoo-Docker] Starting Apache2!"

exec /usr/sbin/apache2ctl -D FOREGROUND
